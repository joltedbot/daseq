# DAW Adjacent Sequencer (daseq)


## Description
A DAW Adjacent (MIDI) Sequencer in Rust for dawless but still hybrid syth setups.

This project is in very early development and doesn't do much just yet.

The src/main.rs file does have a few examples of how some of the existing functionality can be called but it is not intended to be used as. 

## Functionality

The funcationality that is being worked on now is as follows.

daseq can currently crate multiple tracks each of which can construct and play a sequence on its own channel based on the supplied "steps".  Which it is not limited in the code the idea is to provide 4 tracks intitially.  The tracks share a common clock that be optionally sent out a midi output port to sync the tracks with hardware or software devices outside of daseq. Each track can define its own number of steps. This is not currently bounded but the plan is to cap at 128 or 256 depending on performance testing. 

A step is a 16th note. The time duration being dicated by the beats per minute (bpm) of the internal clock. A step contains both a note and a midi control change (cc) component. 

The step's note component can be either a rest, a single note, or a chord with up to 16 notes. Steps, and with that their notes can be tied together to make arbitrary length notes (2 steps tied is an 1/8, 3 steps tied is a dotted 1/8th, 4 steps tied is a quater note etc.). 

Tied steps can be of types TieStart, TieHold, and TieEnd.  To make an 8th note you send the same note value and velocity for 2 steps first a TieStart and then a TieEnd. TieHold simply allows the note to keep playing through the full length of the step. Effectively just a spacer between a TieStart and is TieEnd. To make a quater note you would send the same note value and velocity to 4 consecutive steps consisitng of - TieStart, TieHold, TieHold, TieEnd. If you do not end the tie the note will ring out until it is stopped or the same note is started and stopped.

Steps also allow an arbitrary midi cc number and value message to be sent on the channel the track is using. For any step you can define one cc to send (the plan is to allow more in the future). You assign any valid the cc number and cc value (ie. 1-127) and it will be sent with the note information for that step. This would allow you to say sweek a filter through the span of the sequence. Not this happens with the steps so the value will jump to the value you provide on each 16th note rather than being continuous.

The clock is started with daseq and is running all the time. You can set the bpm and change it on the fly. The change taking effect by the next 16th note step.  You can also enable and disable the clock sending outside daseq effectively stoping the clock as far as the outside world is concerned. Tracks can also be independently started and stopped wihtout impacting the state of the clock.

Transport controls also exist and can be send on any midi channel including start, stop, and continue from the last position.

A midi panic can be send by using the method that sends All Notes Off to all 16 possible midi channels.