use crate::{cc::ControlChange, midi::note::Note};
use serde::{Deserialize, Serialize};

#[derive(Clone, Default, Debug, PartialEq, Serialize, Deserialize)]
pub struct Step {
    pub note: Note,
    pub control_change: Option<ControlChange>,
}

impl Step {
    pub fn new(note: Note, control_change: Option<ControlChange>) -> Self {
        Self {
            note,
            control_change,
        }
    }
}
