#![allow(dead_code)]

pub mod cc;
pub mod note;

use midir::{MidiOutput, MidiOutputConnection, SendError};
use std::error::Error;
use std::time::{Duration, Instant};

use crate::errors::LocalError;
use crate::midi::cc::ControlChange;
use crate::midi::note::Kind;
use crate::midi::note::Note;

const MIDI_CONNECTION_NAME: &str = "get_ports";
const SPIN_SLEEP_NATIVE_ACCURACY: u32 = 10000000;
pub const MIN_BEATS_PER_MINUTE: u16 = 30;
pub const MAX_BEATS_PER_MINUTE: u16 = 400;
const MIDI_START_MESSAGE: u8 = 0xFA;
const MIDI_CONTINUE_MESSAGE: u8 = 0xFB;
const MIDI_STOP_MESSAGE: u8 = 0xFC;
const CLOCK_MESSAGE: u8 = 0xF8;

// Note the 0 based midi channel values expected by the underling midi connections
// Users will expect to supply channels 1 - 16 so off by 1 errors need to be handled by callers of this module
const MIN_MIDI_CHANNEL: u8 = 0;
const MAX_MIDI_CHANNEL: u8 = 15;

pub struct Midi {
    pub output_connection: MidiOutputConnection,
}

impl Midi {
    pub fn new(connection_name: &str, output_port_number: usize) -> Result<Self, Box<dyn Error>> {
        let output = MidiOutput::new(connection_name)?;

        if output.ports().is_empty() {
            return Err(LocalError::MidiNoOutputPort.into());
        }

        if output_port_number > output.ports().len() {
            return Err(LocalError::MidiPortOutOfRange(output_port_number).into());
        }

        let output_port = output.ports()[output_port_number].clone();
        let output_connection = output.connect(&output_port, connection_name)?;

        Ok(Self { output_connection })
    }

    pub fn send_all_notes_off(&mut self) -> Result<(), SendError> {
        for channel in MIN_MIDI_CHANNEL..=MAX_MIDI_CHANNEL {
            self.output_connection.send(&cc::panic_message(channel))?;
        }

        Ok(())
    }

    pub fn send_note(
        &mut self,
        channel: u8,
        note: Note,
        duration_in_microseconds: u64,
    ) -> Result<(), Box<dyn Error>> {
        validate_midi_channel_is_in_range(channel)?;

        match note.kind {
            Kind::Rest => {}
            Kind::SingleStep => self.send_single_note(channel, note, duration_in_microseconds)?,
            Kind::TieStart | Kind::TieHold | Kind::TieEnd => {
                self.send_tie(channel, note, duration_in_microseconds)?
            }
        }
        Ok(())
    }

    fn send_single_note(
        &mut self,
        channel: u8,
        note: Note,
        duration_in_microseconds: u64,
    ) -> Result<(), Box<dyn Error>> {
        let processing_timer = Instant::now();

        for single_note in note.clone().on_message(channel) {
            self.output_connection.send(&single_note)?;
        }

        let spin_sleeper = spin_sleep::SpinSleeper::new(SPIN_SLEEP_NATIVE_ACCURACY)
            .with_spin_strategy(spin_sleep::SpinStrategy::SpinLoopHint);

        spin_sleeper
            .sleep(Duration::from_micros(duration_in_microseconds) - (processing_timer.elapsed()));

        for single_note in note.clone().off_message(channel) {
            self.output_connection.send(&single_note)?;
        }

        Ok(())
    }

    fn send_tie(
        &mut self,
        channel: u8,
        note: Note,
        duration_in_microseconds: u64,
    ) -> Result<(), SendError> {
        let processing_timer = Instant::now();

        let spin_sleeper = spin_sleep::SpinSleeper::new(SPIN_SLEEP_NATIVE_ACCURACY)
            .with_spin_strategy(spin_sleep::SpinStrategy::SpinLoopHint);

        match note.kind {
            Kind::TieStart => {
                for single_note in note.clone().on_message(channel) {
                    self.output_connection.send(&single_note)?;
                }

                spin_sleeper.sleep(
                    Duration::from_micros(duration_in_microseconds) - (processing_timer.elapsed()),
                );
            }

            Kind::TieHold => {
                spin_sleeper.sleep(
                    Duration::from_micros(duration_in_microseconds) - (processing_timer.elapsed()),
                );
            }

            Kind::TieEnd => {
                spin_sleeper.sleep(
                    Duration::from_micros(duration_in_microseconds) - (processing_timer.elapsed()),
                );

                for single_note in note.clone().off_message(channel) {
                    self.output_connection.send(&single_note)?;
                }
            }

            _ => {}
        }

        Ok(())
    }

    pub fn send_cc(&mut self, channel: u8, cc: ControlChange) -> Result<(), Box<dyn Error>> {
        validate_midi_channel_is_in_range(channel)?;

        self.output_connection
            .send(&cc.control_change_message(channel))?;

        Ok(())
    }

    pub fn send_clock_pulse(&mut self) -> Result<(), SendError> {
        self.output_connection.send(&[CLOCK_MESSAGE])?;

        Ok(())
    }

    pub fn send_transport_start(&mut self) -> Result<(), SendError> {
        self.output_connection.send(&[MIDI_START_MESSAGE])?;

        Ok(())
    }

    pub fn send_transport_stop(&mut self) -> Result<(), SendError> {
        self.output_connection.send(&[MIDI_STOP_MESSAGE])?;

        Ok(())
    }

    pub fn send_transport_continue_from_last_position(&mut self) -> Result<(), SendError> {
        self.output_connection.send(&[MIDI_CONTINUE_MESSAGE])?;

        Ok(())
    }
}

pub fn list_midi_output_ports() -> Result<Vec<(usize, String)>, Box<dyn Error>> {
    let mut output_ports: Vec<(usize, String)> = vec![];
    let midi_out = MidiOutput::new(MIDI_CONNECTION_NAME)?;

    match midi_out.ports().len() {
        0 => Err(LocalError::MidiNoOutputPort.into()),
        _ => {
            for (port_id, port) in midi_out.ports().iter().enumerate() {
                output_ports.push((port_id, midi_out.port_name(port)?));
            }
            Ok(output_ports)
        }
    }
}

pub fn validate_beats_per_minute_is_in_range(bpm: u16) -> Result<(), LocalError> {
    if !(MIN_BEATS_PER_MINUTE..=MAX_BEATS_PER_MINUTE).contains(&bpm) {
        return Err(LocalError::ClockBPMOutOfRange {
            min: MIN_BEATS_PER_MINUTE,
            max: MAX_BEATS_PER_MINUTE,
        });
    }

    Ok(())
}

pub fn validate_midi_channel_is_in_range(channel: u8) -> Result<(), LocalError> {
    if !(MIN_MIDI_CHANNEL..=MAX_MIDI_CHANNEL).contains(&channel) {
        return Err(LocalError::MidiChannelOutOfRange {
            min: MIN_MIDI_CHANNEL,
            max: MAX_MIDI_CHANNEL,
        });
    }

    Ok(())
}

#[cfg(test)]
mod tests {
    use super::*;
    use midir::os::unix::VirtualOutput;

    #[test]
    fn new_fail_bad_midi_output_port_number() {
        let result = Midi::new("test_connection", 1000);
        assert!(result.is_err());
    }

    #[test]
    fn send_fail_channel_range_too_high() {
        let mut midi_test = midi_test_object();
        let result = midi_test.send_note(MAX_MIDI_CHANNEL + 1, Default::default(), 20833);
        assert!(result.is_err());
    }

    #[test]
    fn send_success_single_note() {
        let mut midi_test = midi_test_object();
        let result = midi_test.send_note(
            12,
            Note::new(note::Kind::SingleStep, vec![(100, 100)]).unwrap(),
            20833,
        );
        assert!(result.is_ok());
    }

    #[test]
    fn send_success_tiestart() {
        let mut midi_test = midi_test_object();
        let result = midi_test.send_note(
            12,
            Note::new(note::Kind::TieStart, vec![(100, 100)]).unwrap(),
            20833,
        );
        assert!(result.is_ok());
    }

    #[test]
    fn send_success_tieshold() {
        let mut midi_test = midi_test_object();
        let result = midi_test.send_note(
            12,
            Note::new(note::Kind::TieHold, vec![(100, 100)]).unwrap(),
            20833,
        );
        assert!(result.is_ok());
    }

    #[test]
    fn send_success_tieend() {
        let mut midi_test = midi_test_object();
        let result = midi_test.send_note(
            12,
            Note::new(note::Kind::TieEnd, vec![(100, 100)]).unwrap(),
            20833,
        );
        assert!(result.is_ok());
    }

    #[test]
    fn send_all_notes_off_success() {
        let mut midi_test = midi_test_object();
        let result = midi_test.send_all_notes_off();
        assert!(result.is_ok());
    }

    #[test]
    fn send_cc_fail_channel_range_too_high() {
        let mut midi_test = midi_test_object();
        let result = midi_test.send_cc(MAX_MIDI_CHANNEL + 1, Default::default());
        assert!(result.is_err());
    }

    #[test]
    fn start_success() {
        let mut test_transport = midi_test_object();
        assert!(test_transport.send_transport_start().is_ok());
    }

    #[test]
    fn stop_success() {
        let mut test_transport = midi_test_object();
        test_transport
            .send_transport_start()
            .expect("Cloud not start test transport");
        assert!(test_transport.send_transport_stop().is_ok());
    }

    #[test]
    fn continue_from_last_position_success() {
        let mut test_transport = midi_test_object();
        assert!(test_transport
            .send_transport_continue_from_last_position()
            .is_ok());
    }

    fn midi_test_object() -> Midi {
        let output = MidiOutput::new("test_output").ok().unwrap();
        let output_connection = output.create_virtual("test_port").ok().unwrap();

        Midi { output_connection }
    }
}
