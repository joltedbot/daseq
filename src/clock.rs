#![allow(dead_code)]

use crate::errors::LocalError;
use crate::midi::Midi;

use crossbeam_channel::{bounded, Receiver, Sender};
use std::collections::HashMap;
use std::error::Error;
use std::thread;
use std::time::{Duration, Instant};

const ERROR_CLOCK_SEND: &str = "Can not send clock pulse";
const ERROR_TRACK_PULSE_SEND: &str = "Can not send the track the clock pulse";
const SPIN_SLEEP_NATIVE_ACCURACY: u32 = 15000000;
const PULSES_PER_QUARTER_NOTE: u64 = 24;
const MIN_BEATS_PER_MINUTE: u16 = 20;
const MAX_BEATS_PER_MINUTE: u16 = 400;

#[derive(Clone, Debug)]
pub struct Clock {
    pub state: State,
    name: String,
    beats_per_minute: u16,
    pulse_duration_in_microseconds: u64,
    internal_clock_pulses: InternalClock,
    external_clock_pulses: ExternalClock,
    clock_command_sender: Option<Sender<RunnerMessages>>,
    pulse_consumer_sender_list: HashMap<String, Sender<bool>>,
    pulse_consumer_receiver_list: HashMap<String, Receiver<bool>>,
}

#[derive(Clone, Default, Debug, PartialEq)]
pub enum State {
    Running,
    #[default]
    Stopped,
}

#[derive(Debug, Clone)]
enum RunnerMessages {
    PulseInterval(Duration),
    AddConsumerSender(String, Sender<bool>),
    RemoveConsumerSender(String),
    StopClock,
    SendInternalPulses,
    StopInternalPulses,
    SendExternalPulses,
    StopExternalPulses,
}

#[derive(Clone, Default, Debug, PartialEq)]
enum InternalClock {
    Enabled,
    #[default]
    Disabled,
}

#[derive(Clone, Default, Debug, PartialEq)]
enum ExternalClock {
    Enabled,
    #[default]
    Disabled,
}

impl Clock {
    pub fn new(name: String, beats_per_minute: u16) -> Result<Self, LocalError> {
        Ok(Self {
            name,
            beats_per_minute,
            pulse_duration_in_microseconds: pulse_duration_microseconds_from_bpm(beats_per_minute)?,
            internal_clock_pulses: InternalClock::Disabled,
            external_clock_pulses: ExternalClock::Disabled,
            clock_command_sender: None,
            state: State::Stopped,
            pulse_consumer_sender_list: HashMap::new(),
            pulse_consumer_receiver_list: HashMap::new(),
        })
    }

    pub fn start(&mut self, midi_output: Midi) -> Result<(), Box<dyn Error>> {
        if self.state == State::Running {
            return Ok(());
        }

        let (clock_thread_message_sender, clock_thread_message_receiver) = bounded(4);
        self.clock_command_sender = Some(clock_thread_message_sender);

        self.clock_runner(midi_output, clock_thread_message_receiver);

        self.state = State::Running;

        Ok(())
    }

    fn clock_runner(
        &self,
        mut midi_output: Midi,
        clock_thread_message_receiver: Receiver<RunnerMessages>,
    ) {
        let mut pulse_consumer_sender_list = self.pulse_consumer_sender_list.clone();
        let mut internal_clock_pulses = self.internal_clock_pulses.clone();
        let mut external_clock_pulses = self.external_clock_pulses.clone();
        let mut pulse_interval = Duration::from_micros(self.pulse_duration_in_microseconds);

        thread::spawn(move || {
            let spin_sleeper = spin_sleep::SpinSleeper::new(SPIN_SLEEP_NATIVE_ACCURACY)
                .with_spin_strategy(spin_sleep::SpinStrategy::YieldThread);

            loop {
                let processing_time = Instant::now();

                let message_queue: Vec<RunnerMessages> =
                    clock_thread_message_receiver.try_iter().collect();

                for message in message_queue {
                    match message {
                        RunnerMessages::SendInternalPulses => {
                            internal_clock_pulses = InternalClock::Enabled;
                        }
                        RunnerMessages::StopInternalPulses => {
                            internal_clock_pulses = InternalClock::Disabled;
                        }
                        RunnerMessages::SendExternalPulses => {
                            external_clock_pulses = ExternalClock::Enabled;
                        }
                        RunnerMessages::StopExternalPulses => {
                            external_clock_pulses = ExternalClock::Disabled;
                        }
                        RunnerMessages::PulseInterval(pulse_interval_duration) => {
                            pulse_interval = pulse_interval_duration;
                        }
                        RunnerMessages::AddConsumerSender(name, sender) => {
                            pulse_consumer_sender_list.insert(name, sender);
                        }
                        RunnerMessages::RemoveConsumerSender(name) => {
                            pulse_consumer_sender_list.remove(&name);
                        }
                        RunnerMessages::StopClock => break,
                    }
                }

                if external_clock_pulses == ExternalClock::Enabled {
                    midi_output.send_clock_pulse().expect(ERROR_CLOCK_SEND);
                }

                if internal_clock_pulses == InternalClock::Enabled {
                    for sender in pulse_consumer_sender_list.values() {
                        if sender.is_empty() {
                            sender.send(true).expect(ERROR_TRACK_PULSE_SEND);
                        }
                    }
                }
                spin_sleeper.sleep(pulse_interval - processing_time.elapsed());
            }
        });
    }

    pub fn stop(&mut self) -> Result<(), Box<dyn Error>> {
        if self.state == State::Stopped {
            return Ok(());
        }

        self.update_clock(RunnerMessages::StopClock)?;
        self.state = State::Stopped;

        Ok(())
    }

    pub fn set_beats_per_minute(&mut self, bpm: u16) -> Result<(), Box<dyn Error>> {
        if !(MIN_BEATS_PER_MINUTE..=MAX_BEATS_PER_MINUTE).contains(&bpm) {
            return Err(LocalError::ClockBPMOutOfRange {
                min: MIN_BEATS_PER_MINUTE,
                max: MAX_BEATS_PER_MINUTE,
            }
            .into());
        }

        self.pulse_duration_in_microseconds = pulse_duration_microseconds_from_bpm(bpm)?;
        self.beats_per_minute = bpm;

        if self.state == State::Stopped {
            Ok(())
        } else {
            self.update_clock(RunnerMessages::PulseInterval(Duration::from_micros(
                self.pulse_duration_in_microseconds,
            )))
        }
    }

    pub fn subscsribe(&mut self, name: String) -> Result<Receiver<bool>, Box<dyn Error>> {
        let (clock_thread_message_sender, clock_thread_message_receiver) = bounded(4);

        if let Some(exisiting_sender) = self
            .pulse_consumer_sender_list
            .insert(name.clone(), clock_thread_message_sender.clone())
        {
            drop(exisiting_sender);
        }

        self.update_clock(RunnerMessages::AddConsumerSender(
            name.clone(),
            clock_thread_message_sender,
        ))?;

        if let Some(exisiting_receiver) = self
            .pulse_consumer_receiver_list
            .insert(name, clock_thread_message_receiver.clone())
        {
            drop(exisiting_receiver);
        }

        if self.internal_clock_pulses == InternalClock::Disabled {
            self.enable_internal_clock_pulses()?;
        }

        Ok(clock_thread_message_receiver)
    }

    pub fn unsubscsribe(&mut self, name: String) -> Result<(), Box<dyn Error>> {
        if let Some(exisiting_sender) = self.pulse_consumer_sender_list.remove(&name.clone()) {
            drop(exisiting_sender);

            self.update_clock(RunnerMessages::RemoveConsumerSender(name.clone()))?;
        }

        if let Some(exisiting_receiver) = self.pulse_consumer_receiver_list.remove(&name) {
            drop(exisiting_receiver);
        }

        if self.pulse_consumer_sender_list.is_empty() {
            self.disable_internal_clock_pulses()?;
        }

        Ok(())
    }

    pub fn enable_external_clock_pulses(&mut self) -> Result<(), Box<dyn Error>> {
        if self.external_clock_pulses == ExternalClock::Enabled {
            return Ok(());
        }

        if self.state == State::Running {
            self.update_clock(RunnerMessages::SendExternalPulses)?;
        }

        self.external_clock_pulses = ExternalClock::Enabled;

        Ok(())
    }

    pub fn disable_external_clock_pulses(&mut self) -> Result<(), Box<dyn Error>> {
        if self.external_clock_pulses == ExternalClock::Disabled {
            return Ok(());
        }

        if self.state == State::Running {
            self.update_clock(RunnerMessages::StopExternalPulses)?;
        }

        self.external_clock_pulses = ExternalClock::Disabled;

        Ok(())
    }

    fn enable_internal_clock_pulses(&mut self) -> Result<(), Box<dyn Error>> {
        if self.internal_clock_pulses == InternalClock::Enabled {
            return Ok(());
        }

        if self.state == State::Running {
            self.update_clock(RunnerMessages::SendInternalPulses)?;
        }

        self.internal_clock_pulses = InternalClock::Enabled;

        Ok(())
    }

    fn disable_internal_clock_pulses(&mut self) -> Result<(), Box<dyn Error>> {
        if self.internal_clock_pulses == InternalClock::Disabled {
            return Ok(());
        }

        if self.state == State::Running {
            self.update_clock(RunnerMessages::StopInternalPulses)?;
        }

        self.internal_clock_pulses = InternalClock::Disabled;

        Ok(())
    }

    fn update_clock(&self, update_value: RunnerMessages) -> Result<(), Box<dyn Error>> {
        let sender = match self.clock_command_sender.clone() {
            Some(sender) => sender,
            None => return Err(LocalError::ClockChannelSender.into()),
        };

        sender.send(update_value)?;

        Ok(())
    }
}

pub fn pulse_duration_microseconds_from_bpm(bpm: u16) -> Result<u64, LocalError> {
    if !(MIN_BEATS_PER_MINUTE..=MAX_BEATS_PER_MINUTE).contains(&bpm) {
        return Err(LocalError::ClockBPMOutOfRange {
            min: MIN_BEATS_PER_MINUTE,
            max: MAX_BEATS_PER_MINUTE,
        });
    }

    // convert bpm -> beats per second -> microseconds per beat -> microseconds per clock pulse
    Ok(((1000000.0 / ((bpm as f64) / 60.0)) / PULSES_PER_QUARTER_NOTE as f64).floor() as u64)
}

#[cfg(test)]
mod tests {
    use super::*;
    use crate::midi::Midi;
    use midir::{os::unix::VirtualOutput, MidiOutput};

    // A mock midi object that allows a virutual MidiOutputConnection passed into methods without needing to have a real midi port
    fn midi_test_object() -> Midi {
        let output = MidiOutput::new("test_output").ok().unwrap();
        let output_connection = output.create_virtual("test_port").ok().unwrap();

        Midi { output_connection }
    }

    #[test]
    fn stop_success_already_stopped() {
        let mut test_clock = Clock::new("Test Clock".to_string(), 120).unwrap();
        test_clock.state = State::Stopped;
        assert!(test_clock.stop().is_ok());
    }

    #[test]
    fn stop_success() {
        let midi_output = midi_test_object();

        let mut test_clock = Clock::new("Test Clock".to_string(), 120).unwrap();
        test_clock.start(midi_output).unwrap();
        thread::sleep(Duration::from_secs(2));

        assert!(test_clock.stop().is_ok());
        assert_eq!(test_clock.state, State::Stopped);
    }

    #[test]
    fn set_beats_per_minute_success_while_runing() {
        let midi_output = midi_test_object();

        let mut test_clock = Clock::new("Test Clock".to_string(), 120).unwrap();
        test_clock.start(midi_output).unwrap();

        assert!(test_clock.set_beats_per_minute(100).is_ok());

        test_clock.stop().unwrap();
    }

    #[test]
    fn set_beats_per_minute_success_while_stopped() {
        let mut test_clock = Clock::new("Test Clock".to_string(), 120).unwrap();
        assert!(test_clock.set_beats_per_minute(100).is_ok());
    }

    #[test]
    fn set_beats_per_minute_fail_range() {
        let midi_output = midi_test_object();

        let mut test_clock = Clock::new("Test Clock".to_string(), 120).unwrap();

        assert!(test_clock
            .set_beats_per_minute(MIN_BEATS_PER_MINUTE - 1)
            .is_err());
        assert!(test_clock
            .set_beats_per_minute(MAX_BEATS_PER_MINUTE + 1)
            .is_err());

        test_clock.stop().unwrap();
    }

    #[test]
    fn set_beats_per_minute_fail_no_sender() {
        let midi_output = midi_test_object();

        let mut test_clock = Clock::new("Test Clock".to_string(), 120).unwrap();
        test_clock.start(midi_output).unwrap();

        let temp_sender = test_clock.clock_command_sender;
        test_clock.clock_command_sender = None;

        assert!(test_clock.set_beats_per_minute(120).is_err());

        test_clock.clock_command_sender = temp_sender;
        test_clock.stop().unwrap();
    }

    #[test]
    fn pulse_duration_microseconds_from_bpm_success() {
        match pulse_duration_microseconds_from_bpm(120) {
            Ok(pulses) => assert_eq!(pulses, 20833),
            Err(e) => panic!("Should have succeeded {:?}", e),
        }
    }

    #[test]
    fn pulse_duration_microseconds_from_bpm_fail_high() {
        match pulse_duration_microseconds_from_bpm(1000) {
            Ok(pulses) => panic!(
                "BPM 1000 should have failed but recieved {:?} pulses",
                pulses
            ),
            Err(e) => assert_eq!(
                e,
                LocalError::ClockBPMOutOfRange {
                    min: MIN_BEATS_PER_MINUTE,
                    max: MAX_BEATS_PER_MINUTE,
                }
            ),
        }
    }

    #[test]
    fn pulse_duration_microseconds_from_bpm_fail_low() {
        match pulse_duration_microseconds_from_bpm(1) {
            Ok(pulses) => panic!("BPM 1 should have failed but recieved {:?} pulses", pulses),
            Err(e) => assert_eq!(
                e,
                LocalError::ClockBPMOutOfRange {
                    min: MIN_BEATS_PER_MINUTE,
                    max: MAX_BEATS_PER_MINUTE,
                }
            ),
        }
    }
}
