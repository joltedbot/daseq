#![allow(dead_code)]

use crate::errors::LocalError;

use serde::{Deserialize, Serialize};

const MIN_VALUE: u8 = 0;
const MAX_VALUE: u8 = 127;
const MIN_VELOCITY: u8 = 0;
const MAX_VELOCITY: u8 = 127;
const NOTE_ON_STATUS: u8 = 0x90;
const NOTE_OFF_STATUS: u8 = 0x80;
const NOTE_OFF_VELOCITY: u8 = 0;
const MAX_NUMBER_OF_NOTES: u8 = 16;

#[derive(Clone, Default, Debug, PartialEq, Serialize, Deserialize)]
pub struct Note {
    pub kind: Kind,
    note_list: Vec<(u8, u8)>, // (midi note value, note velocity)
}

#[derive(Clone, Default, Debug, PartialEq, Serialize, Deserialize)]
pub enum Kind {
    #[default]
    Rest,
    SingleStep,
    TieStart,
    TieHold,
    TieEnd,
}

impl Note {
    pub fn new(kind: Kind, note_list: Vec<(u8, u8)>) -> Result<Self, LocalError> {
        validate_notes(&note_list)?;

        Ok(Self { kind, note_list })
    }

    pub fn on_message(&self, channel: u8) -> Vec<[u8; 3]> {
        let mut chord_note_message: Vec<[u8; 3]> = vec![];

        for note in self.note_list.clone() {
            chord_note_message.push([NOTE_ON_STATUS + channel, note.0, note.1]);
        }

        chord_note_message
    }

    pub fn off_message(&self, channel: u8) -> Vec<[u8; 3]> {
        let mut chord_note_message: Vec<[u8; 3]> = vec![];

        for note in self.note_list.clone() {
            chord_note_message.push([NOTE_OFF_STATUS + channel, note.0, NOTE_OFF_VELOCITY]);
        }

        chord_note_message
    }
}

fn validate_notes(note_list: &Vec<(u8, u8)>) -> Result<(), LocalError> {
    if note_list.len() as u8 > MAX_NUMBER_OF_NOTES {
        return Err(LocalError::ChordHasTooManyNotes(MAX_NUMBER_OF_NOTES));
    }

    for note in note_list {
        if note.0 > MAX_VALUE {
            return Err(LocalError::NoteValueOutOfRange {
                min: MIN_VALUE,
                max: MAX_VALUE,
            });
        }

        if note.1 > MAX_VELOCITY {
            return Err(LocalError::NoteVelocityOutOfRange {
                min: MIN_VELOCITY,
                max: MAX_VELOCITY,
            });
        }
    }

    Ok(())
}

#[cfg(test)]
mod tests {
    use super::*;

    const VALID_VALUE: u8 = 5;
    const VALID_VELOCITY: u8 = 100;

    #[test]
    fn validate_notes_fail_value_too_high() {
        assert_eq!(
            validate_notes(&vec![(MAX_VALUE + 1, VALID_VELOCITY)]),
            Err(LocalError::NoteValueOutOfRange {
                min: MIN_VALUE,
                max: MAX_VALUE,
            })
        );
    }

    #[test]
    fn validate_notes_fail_velocity_too_high() {
        assert_eq!(
            validate_notes(&vec![(VALID_VALUE, MAX_VELOCITY + 1)]),
            Err(LocalError::NoteVelocityOutOfRange {
                min: MIN_VELOCITY,
                max: MAX_VELOCITY,
            })
        );
    }

    #[test]
    fn validate_notes_fail_too_many_notes() {
        let chord = [(1, 1); 17].to_vec();

        assert_eq!(
            validate_notes(&chord),
            Err(LocalError::ChordHasTooManyNotes(MAX_NUMBER_OF_NOTES))
        );
    }
}
