#![allow(dead_code)]

use crate::errors::LocalError;
use serde::{Deserialize, Serialize};

const MIN_NUMBER: u8 = 0;
const MAX_NUMBER: u8 = 127;
const MIN_VALUE: u8 = 0;
const MAX_VALUE: u8 = 127;
const CONTROL_CHANGE_STATUS_BYTE: u8 = 0xB0;
const ALL_NOTES_OFF_VALUE: u8 = 0x7B;
const ALL_NOTES_OFF_VELOCITY: u8 = 0x00;

#[derive(Clone, Debug, Default, PartialEq, Serialize, Deserialize)]
pub struct ControlChange {
    number: u8,
    value: u8,
}

impl ControlChange {
    pub fn new(number: u8, value: u8) -> Result<Self, LocalError> {
        if number > MAX_NUMBER {
            return Err(LocalError::CCNumberOutOfRange {
                min: MIN_NUMBER,
                max: MAX_NUMBER,
            });
        }

        if value > MAX_VALUE {
            return Err(LocalError::NoteValueOutOfRange {
                min: MIN_VALUE,
                max: MAX_VALUE,
            });
        }

        Ok(Self { number, value })
    }

    pub fn control_change_message(&self, channel: u8) -> [u8; 3] {
        [
            CONTROL_CHANGE_STATUS_BYTE + channel,
            self.number,
            self.value,
        ]
    }
}

pub fn panic_message(channel: u8) -> [u8; 3] {
    [
        CONTROL_CHANGE_STATUS_BYTE + channel,
        ALL_NOTES_OFF_VALUE,
        ALL_NOTES_OFF_VELOCITY,
    ]
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn new_success() {
        let expected_cc = ControlChange {
            number: 100,
            value: 100,
        };
        let cc_test = ControlChange::new(100, 100).unwrap();
        assert_eq!(cc_test, expected_cc);
    }

    #[test]
    fn new_fail_cc_number_range_too_high() {
        let cc_test = ControlChange::new(MAX_NUMBER + 1, 100);
        assert!(cc_test.is_err());
    }

    #[test]
    fn new_fail_cc_value_range_too_high() {
        let cc_test = ControlChange::new(100, MAX_VALUE + 1);
        assert!(cc_test.is_err());
    }
}
