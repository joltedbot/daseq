#![allow(dead_code)]

use thiserror::Error;

#[derive(Debug, Error, PartialEq, Eq)]
pub enum LocalError {
    // Midi Errors
    #[error("There is no midi output port available")]
    MidiNoOutputPort,

    #[error("MidiPort {0} does not exist")]
    MidiPortOutOfRange(usize),

    #[error("The midi channel is outside the range of {min} to {max}")]
    MidiChannelOutOfRange { min: u8, max: u8 },

    #[error("The supplied duty duty cycle {supplied} exceeds the max {max}")]
    MidiDutyCycleOutOfRange { supplied: u8, max: u8 },

    #[error("Incorrect note kind provided")]
    MidiNoteKindIncorrect(usize),

    // Note Error
    #[error("The midi note value is outside the range of {min} to {max}")]
    NoteValueOutOfRange { min: u8, max: u8 },

    #[error("The midi note value is outside the range of {min} to {max}")]
    NoteVelocityOutOfRange { min: u8, max: u8 },

    #[error("Too many notes. Chords can have a maximum of {0}")]
    ChordHasTooManyNotes(u8),

    // CC Error
    #[error("The midi CC number is outside the range of {min} to {max}")]
    CCNumberOutOfRange { min: u8, max: u8 },

    #[error("The midi CC value is outside the range of {min} to {max}")]
    CCValueOutOfRange { min: u8, max: u8 },

    // Clock Errors
    #[error("The clock is not running")]
    ClockNotRuning,

    #[error("The clock channel sender is not set")]
    ClockChannelSender,

    #[error("The supplied bpm is outside the range of {min} to {max}")]
    ClockBPMOutOfRange { min: u16, max: u16 },

    #[error("The clock thread paniced while being stopped")]
    ClockThreadPanicOnStop,

    // Track Errors
    #[error("Supplied Step {0} does not exist")]
    StepDoesNotExist(u8),

    #[error("The track is not running")]
    TrackNotRuning,

    #[error("The track is already running")]
    TrackAlreadyRuning,

    #[error("The track channel sender is not set")]
    TrackNoChannelSenderFound,

    #[error("The supplied number of steps is outside the range of {min} to {max}")]
    TrackStepsOutOfRange { min: u8, max: u8 },

    #[error("One or more track threads did not stop gracefully")]
    TrackThreadStopFailed,

    // Save Errors
    #[error("A saved project named {0} already exist. Please choose another name")]
    SaveProjectAlreadyExists(String),

    #[error("Saved project named {0} not found")]
    SavedProjectNameDoesNotExist(String),

    #[error("A saved track named {0} already exist. Please choose another name")]
    SaveTrackAlreadyExists(String),

    #[error("Saved track named {0} not found")]
    SavedTrackNameDoesNotExist(String),
}
