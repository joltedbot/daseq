#![allow(unused_variables)]
mod clock;
mod errors;
mod midi;
mod project;
mod track;

use crate::midi::*;
use crate::project::*;
use crate::track::*;
use cc::ControlChange;
use step::Step;

use std::io;
use std::thread;
use std::time::Duration;

fn main() {
    println!("DAW Adjacent Sequencer!");

    // Sample usage of some of the currenly implemented core functionality

    /////// Configuration parameters
    let physical_midi_output_port_number = midi_output_port_number();
    let beats_per_minute = 150;
    let track_runtime_in_seconds = 5;

    // Step 1 - Create a project
    let mut test_project =
        Project::new(physical_midi_output_port_number).expect("Could note create the project!");

    // Step 2 set the steps in the sequence for each track
    // Use throw away function at the bottom of the page to setup the tracks steps to the desired notes and cc values.
    test_project.track_list[0]
        .set_all_steps(track1_steps())
        .expect("Could not set track 1 notes");

    test_project.track_list[1]
        .set_all_steps(track2_steps())
        .expect("Could not set track 2 notes");

    test_project.track_list[2]
        .set_all_steps(track3_steps())
        .expect("Could not set track 3 notes");

    test_project.track_list[3]
        .set_all_steps(track4_steps())
        .expect("Could not set track 4 notes");

    // Optional step 2.5 enable sending clock pulses to external devices
    /*
    test_project
        .master_clock
        .enable_external_clock_pulses()
        .expect("Could not enable external clock");
    */

    // Step 3 Start the tracks
    let handles = test_project
        .start_all_tracks()
        .expect("Could not start tracks");

    // Step 4 Let it run and listen to your handywork
    thread::sleep(Duration::from_secs(track_runtime_in_seconds));

    // Step 5 stop the tracks
    test_project
        .stop_all_tracks(handles)
        .expect("Could not start tracks");
}

////////////////////////////////////////////////////////////////////////
///////////  Throwaway Utility Functions For Testing Main   ///////////
//////////////////////////////////////////////////////////////////////

fn midi_output_port_number() -> usize {
    let mut argv = std::env::args();

    // Step 1 - Check the available output ports.
    let mut buffer = String::new();

    if argv.len() > 1 {
        buffer = argv.nth(1).unwrap();
        return buffer.trim().parse().unwrap();
    } else {
        // This method returns the list so you can get the index of the port you need to feed to other functions
        match list_midi_output_ports() {
            Ok(list) if list.len() == 1 => list[0].0,
            Ok(list) => {
                println!("----------------------------");
                println!("Available Midi Output Ports");
                println!("----------------------------");
                println!("  ID  -    Port Name        ");
                println!("----------------------------");
                for port in list {
                    println!("-> {:?} - {:?}", port.0, port.1);
                }
                println!("----------------------------");
                let stdin = io::stdin();

                println!("\nPlease supply the midi port ID to use for this test: ");
                stdin
                    .read_line(&mut buffer)
                    .expect("Could not read input line");
                buffer.trim().parse().unwrap()
            }
            Err(_) => panic!("\n\nNo midi output ports available\n\n"),
        }
    }
}

// Just a throw away utility function to allow setting track steps to specific notes. Not really part of the applcation.
fn track1_steps() -> Vec<Step> {
    vec![
        Step::new(
            note::Note::new(
                note::Kind::TieStart,
                vec![(59, 100), (64, 100), (67, 100), (71, 100)],
            )
            .unwrap(),
            Some(ControlChange::new(17, 10).unwrap()),
        ),
        Step::new(
            note::Note::new(
                note::Kind::TieEnd,
                vec![(59, 100), (64, 100), (67, 100), (71, 100)],
            )
            .unwrap(),
            Some(ControlChange::new(17, 20).unwrap()),
        ),
        Step::new(
            note::Note::new(
                note::Kind::TieStart,
                vec![(59, 100), (64, 100), (67, 100), (71, 100)],
            )
            .unwrap(),
            Some(ControlChange::new(17, 30).unwrap()),
        ),
        Step::new(
            note::Note::new(
                note::Kind::TieEnd,
                vec![(59, 100), (64, 100), (67, 100), (71, 100)],
            )
            .unwrap(),
            Some(ControlChange::new(17, 40).unwrap()),
        ),
        Step::new(
            note::Note::new(note::Kind::SingleStep, vec![(64, 100)]).unwrap(),
            Some(ControlChange::new(17, 50).unwrap()),
        ),
        Step::new(
            note::Note::new(note::Kind::SingleStep, vec![(66, 100)]).unwrap(),
            Some(ControlChange::new(17, 60).unwrap()),
        ),
        Step::new(
            note::Note::new(note::Kind::SingleStep, vec![(69, 127)]).unwrap(),
            Some(ControlChange::new(17, 70).unwrap()),
        ),
        Step::new(
            note::Note::new(note::Kind::Rest, vec![(0, 0)]).unwrap(),
            Some(ControlChange::new(17, 80).unwrap()),
        ),
        Step::new(
            note::Note::new(note::Kind::SingleStep, vec![(64, 100)]).unwrap(),
            Some(ControlChange::new(17, 90).unwrap()),
        ),
        Step::new(
            note::Note::new(note::Kind::SingleStep, vec![(66, 100)]).unwrap(),
            Some(ControlChange::new(17, 100).unwrap()),
        ),
        Step::new(
            note::Note::new(note::Kind::SingleStep, vec![(71, 127)]).unwrap(),
            Some(ControlChange::new(17, 110).unwrap()),
        ),
        Step::new(
            note::Note::new(note::Kind::Rest, vec![(0, 0)]).unwrap(),
            Some(ControlChange::new(17, 120).unwrap()),
        ),
        Step::new(
            note::Note::new(
                note::Kind::TieStart,
                vec![(59, 100), (64, 100), (67, 100), (71, 100)],
            )
            .unwrap(),
            Some(ControlChange::new(17, 110).unwrap()),
        ),
        Step::new(
            note::Note::new(
                note::Kind::TieEnd,
                vec![(59, 100), (64, 100), (67, 100), (71, 100)],
            )
            .unwrap(),
            Some(ControlChange::new(17, 100).unwrap()),
        ),
        Step::new(
            note::Note::new(
                note::Kind::TieStart,
                vec![(59, 100), (64, 100), (67, 100), (71, 100)],
            )
            .unwrap(),
            Some(ControlChange::new(17, 90).unwrap()),
        ),
        Step::new(
            note::Note::new(
                note::Kind::TieEnd,
                vec![(59, 100), (64, 100), (67, 100), (71, 100)],
            )
            .unwrap(),
            Some(ControlChange::new(17, 80).unwrap()),
        ),
        Step::new(
            note::Note::new(note::Kind::SingleStep, vec![(64, 100)]).unwrap(),
            Some(ControlChange::new(17, 70).unwrap()),
        ),
        Step::new(
            note::Note::new(note::Kind::SingleStep, vec![(66, 100)]).unwrap(),
            Some(ControlChange::new(17, 60).unwrap()),
        ),
        Step::new(
            note::Note::new(note::Kind::SingleStep, vec![(69, 100)]).unwrap(),
            Some(ControlChange::new(17, 50).unwrap()),
        ),
        Step::new(
            note::Note::new(note::Kind::SingleStep, vec![(64, 100)]).unwrap(),
            Some(ControlChange::new(17, 40).unwrap()),
        ),
        Step::new(
            note::Note::new(note::Kind::SingleStep, vec![(66, 100)]).unwrap(),
            Some(ControlChange::new(17, 30).unwrap()),
        ),
        Step::new(
            note::Note::new(note::Kind::SingleStep, vec![(71, 100)]).unwrap(),
            Some(ControlChange::new(17, 20).unwrap()),
        ),
    ]
}

fn track2_steps() -> Vec<Step> {
    vec![
        Step::new(
            note::Note::new(note::Kind::TieStart, vec![(71, 100)]).unwrap(),
            Some(ControlChange::new(17, 10).unwrap()),
        ),
        Step::new(
            note::Note::new(note::Kind::TieEnd, vec![(71, 100)]).unwrap(),
            Some(ControlChange::new(17, 20).unwrap()),
        ),
        Step::new(
            note::Note::new(note::Kind::TieStart, vec![(71, 100)]).unwrap(),
            Some(ControlChange::new(17, 30).unwrap()),
        ),
        Step::new(
            note::Note::new(note::Kind::TieEnd, vec![(71, 100)]).unwrap(),
            Some(ControlChange::new(17, 40).unwrap()),
        ),
        Step::new(
            note::Note::new(note::Kind::SingleStep, vec![(76, 100)]).unwrap(),
            Some(ControlChange::new(17, 50).unwrap()),
        ),
        Step::new(
            note::Note::new(note::Kind::SingleStep, vec![(78, 100)]).unwrap(),
            Some(ControlChange::new(17, 60).unwrap()),
        ),
        Step::new(
            note::Note::new(note::Kind::SingleStep, vec![(81, 127)]).unwrap(),
            Some(ControlChange::new(17, 70).unwrap()),
        ),
        Step::new(
            note::Note::new(note::Kind::Rest, vec![(0, 0)]).unwrap(),
            Some(ControlChange::new(17, 80).unwrap()),
        ),
        Step::new(
            note::Note::new(note::Kind::SingleStep, vec![(76, 100)]).unwrap(),
            Some(ControlChange::new(17, 90).unwrap()),
        ),
        Step::new(
            note::Note::new(note::Kind::SingleStep, vec![(78, 100)]).unwrap(),
            Some(ControlChange::new(17, 100).unwrap()),
        ),
        Step::new(
            note::Note::new(note::Kind::SingleStep, vec![(83, 127)]).unwrap(),
            Some(ControlChange::new(17, 110).unwrap()),
        ),
        Step::new(
            note::Note::new(note::Kind::Rest, vec![(0, 0)]).unwrap(),
            Some(ControlChange::new(17, 120).unwrap()),
        ),
        Step::new(
            note::Note::new(note::Kind::TieStart, vec![(71, 100)]).unwrap(),
            Some(ControlChange::new(17, 110).unwrap()),
        ),
        Step::new(
            note::Note::new(note::Kind::TieEnd, vec![(71, 100)]).unwrap(),
            Some(ControlChange::new(17, 100).unwrap()),
        ),
        Step::new(
            note::Note::new(note::Kind::TieStart, vec![(71, 100)]).unwrap(),
            Some(ControlChange::new(17, 90).unwrap()),
        ),
        Step::new(
            note::Note::new(note::Kind::TieEnd, vec![(71, 100)]).unwrap(),
            Some(ControlChange::new(17, 80).unwrap()),
        ),
        Step::new(
            note::Note::new(note::Kind::SingleStep, vec![(76, 100)]).unwrap(),
            Some(ControlChange::new(17, 70).unwrap()),
        ),
        Step::new(
            note::Note::new(note::Kind::SingleStep, vec![(78, 100)]).unwrap(),
            Some(ControlChange::new(17, 60).unwrap()),
        ),
        Step::new(
            note::Note::new(note::Kind::SingleStep, vec![(81, 100)]).unwrap(),
            Some(ControlChange::new(17, 50).unwrap()),
        ),
        Step::new(
            note::Note::new(note::Kind::SingleStep, vec![(76, 100)]).unwrap(),
            Some(ControlChange::new(17, 40).unwrap()),
        ),
        Step::new(
            note::Note::new(note::Kind::SingleStep, vec![(78, 100)]).unwrap(),
            Some(ControlChange::new(17, 30).unwrap()),
        ),
        Step::new(
            note::Note::new(note::Kind::SingleStep, vec![(83, 100)]).unwrap(),
            Some(ControlChange::new(17, 20).unwrap()),
        ),
    ]
}

// Just a throw away utility function to allow setting track steps to specific notes. Not really part of the applcation.
fn track3_steps() -> Vec<Step> {
    vec![
        Step::new(
            note::Note::new(note::Kind::TieStart, vec![(35, 100)]).unwrap(),
            None,
        ),
        Step::new(
            note::Note::new(note::Kind::TieEnd, vec![(35, 100)]).unwrap(),
            None,
        ),
        Step::new(
            note::Note::new(note::Kind::TieStart, vec![(38, 100)]).unwrap(),
            None,
        ),
        Step::new(
            note::Note::new(note::Kind::TieEnd, vec![(38, 100)]).unwrap(),
            None,
        ),
        Step::new(
            note::Note::new(note::Kind::TieStart, vec![(42, 100)]).unwrap(),
            None,
        ),
        Step::new(
            note::Note::new(note::Kind::TieEnd, vec![(42, 100)]).unwrap(),
            None,
        ),
        Step::new(
            note::Note::new(note::Kind::TieStart, vec![(45, 100)]).unwrap(),
            None,
        ),
        Step::new(
            note::Note::new(note::Kind::TieEnd, vec![(45, 100)]).unwrap(),
            None,
        ),
        Step::new(
            note::Note::new(note::Kind::TieStart, vec![(47, 100)]).unwrap(),
            None,
        ),
        Step::new(
            note::Note::new(note::Kind::TieEnd, vec![(47, 100)]).unwrap(),
            None,
        ),
        Step::new(
            note::Note::new(note::Kind::TieStart, vec![(45, 100)]).unwrap(),
            None,
        ),
        Step::new(
            note::Note::new(note::Kind::TieEnd, vec![(45, 100)]).unwrap(),
            None,
        ),
        Step::new(
            note::Note::new(note::Kind::TieStart, vec![(42, 100)]).unwrap(),
            None,
        ),
        Step::new(
            note::Note::new(note::Kind::TieEnd, vec![(42, 100)]).unwrap(),
            None,
        ),
        Step::new(
            note::Note::new(note::Kind::TieStart, vec![(38, 100)]).unwrap(),
            None,
        ),
        Step::new(
            note::Note::new(note::Kind::TieEnd, vec![(38, 100)]).unwrap(),
            None,
        ),
    ]
}

fn track4_steps() -> Vec<Step> {
    vec![
        Step::new(
            note::Note::new(note::Kind::TieStart, vec![(47, 100)]).unwrap(),
            None,
        ),
        Step::new(
            note::Note::new(note::Kind::TieEnd, vec![(47, 100)]).unwrap(),
            None,
        ),
        Step::new(
            note::Note::new(note::Kind::TieStart, vec![(45, 100)]).unwrap(),
            None,
        ),
        Step::new(
            note::Note::new(note::Kind::TieEnd, vec![(45, 100)]).unwrap(),
            None,
        ),
        Step::new(
            note::Note::new(note::Kind::TieStart, vec![(42, 100)]).unwrap(),
            None,
        ),
        Step::new(
            note::Note::new(note::Kind::TieEnd, vec![(42, 100)]).unwrap(),
            None,
        ),
        Step::new(
            note::Note::new(note::Kind::TieStart, vec![(38, 100)]).unwrap(),
            None,
        ),
        Step::new(
            note::Note::new(note::Kind::TieEnd, vec![(38, 100)]).unwrap(),
            None,
        ),
        Step::new(
            note::Note::new(note::Kind::TieStart, vec![(35, 100)]).unwrap(),
            None,
        ),
        Step::new(
            note::Note::new(note::Kind::TieEnd, vec![(35, 100)]).unwrap(),
            None,
        ),
        Step::new(
            note::Note::new(note::Kind::TieStart, vec![(38, 100)]).unwrap(),
            None,
        ),
        Step::new(
            note::Note::new(note::Kind::TieEnd, vec![(38, 100)]).unwrap(),
            None,
        ),
        Step::new(
            note::Note::new(note::Kind::TieStart, vec![(42, 100)]).unwrap(),
            None,
        ),
        Step::new(
            note::Note::new(note::Kind::TieEnd, vec![(42, 100)]).unwrap(),
            None,
        ),
        Step::new(
            note::Note::new(note::Kind::TieStart, vec![(45, 100)]).unwrap(),
            None,
        ),
        Step::new(
            note::Note::new(note::Kind::TieEnd, vec![(45, 100)]).unwrap(),
            None,
        ),
    ]
}
