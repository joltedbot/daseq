#![allow(dead_code)]

use crossbeam_channel::Receiver;

use crate::clock::Clock;
use crate::errors::LocalError;
use crate::midi::Midi;
use crate::track::Track;
use crate::TrackStateToSave;

use serde::{Deserialize, Serialize};
use std::error::Error;
use std::thread::JoinHandle;
use std::time::{SystemTime, SystemTimeError, UNIX_EPOCH};

const NUMBER_OF_TRACKS_PER_PROJECT: u8 = 4;
const DEFAULT_BEATS_PER_MINUTE: u16 = 120;
const DEFAULT_NUMBER_OF_STEPS_PER_TRACK: u8 = 1;
const PROJECT_CLOCK_NAME: &str = "Project Clock";
const STOP_ALL_NOTE_MIDI_CONNECTION_NAME: &str = "Stop All Notes";

#[derive(Debug, Clone)]
pub struct Project {
    pub name: String,
    pub beats_per_minute: u16,
    pub master_clock: Clock,
    pub track_list: Vec<Track>,
    midi_output_port_number: usize,
    track_clock_receivers: Vec<Receiver<bool>>,
}

#[derive(Clone, Debug, Serialize, Deserialize)]
pub struct ProjectStateToSave {
    pub save_name: String,
    pub beats_per_minute: u16,
    pub track_list: Vec<(u8, TrackStateToSave)>, // (Midi Channel, Track Save Data)
}

impl Project {
    pub fn new(midi_output_port_number: usize) -> Result<Self, Box<dyn Error>> {
        let clock_midi = Midi::new(PROJECT_CLOCK_NAME, midi_output_port_number)?;

        let mut master_clock =
            Clock::new(PROJECT_CLOCK_NAME.to_string(), DEFAULT_BEATS_PER_MINUTE)?;

        master_clock
            .start(clock_midi)
            .expect("Could not start clock");

        let mut tracks: Vec<Track> = vec![];
        let mut track_clock_receivers: Vec<Receiver<bool>> = vec![];
        let track_thread_join_handles: Vec<JoinHandle<()>> = vec![];

        for track_number in 0..NUMBER_OF_TRACKS_PER_PROJECT {
            let new_track = Track::new(
                track_number.to_string(),
                midi_output_port_number,
                DEFAULT_NUMBER_OF_STEPS_PER_TRACK,
                track_number,
            )?;

            track_clock_receivers.push(master_clock.subscsribe(new_track.name.clone()).unwrap());
            tracks.push(new_track);
        }

        Ok(Self {
            name: generate_random_project_name()?,
            beats_per_minute: DEFAULT_BEATS_PER_MINUTE,
            midi_output_port_number,
            track_list: tracks,
            track_clock_receivers,
            master_clock,
        })
    }

    pub fn start_all_tracks(&mut self) -> Result<Vec<JoinHandle<()>>, Box<dyn Error>> {
        let mut track_joinhandles: Vec<JoinHandle<()>> = vec![];

        for track_id in 0..self.track_list.len() {
            track_joinhandles.push(
                self.track_list[track_id].start(self.track_clock_receivers[track_id].clone())?,
            );
        }

        Ok(track_joinhandles)
    }

    pub fn stop_all_tracks(
        &mut self,
        track_joinhandles: Vec<JoinHandle<()>>,
    ) -> Result<(), Box<dyn Error>> {
        for track_id in 0..self.track_list.len() {
            self.track_list[track_id].stop()?;
        }

        let mut join_failed = false;

        for handle in track_joinhandles {
            join_failed = handle.join().is_err();
        }

        match join_failed {
            false => Ok(()),
            true => Err(Box::new(LocalError::TrackThreadStopFailed)),
        }
    }

    pub fn get_state_to_save(&self) -> ProjectStateToSave {
        let mut track_saved_states: Vec<(u8, TrackStateToSave)> = vec![];

        for track in self.track_list.iter().enumerate() {
            track_saved_states.push((
                track.1.midi_channel,
                track.1.get_state_to_save(track.1.name.clone()),
            ));
        }

        ProjectStateToSave {
            save_name: self.name.clone(),
            beats_per_minute: self.beats_per_minute,
            track_list: track_saved_states,
        }
    }

    pub fn load_saved_state(&mut self, track_state: ProjectStateToSave) -> Result<(), LocalError> {
        self.name = track_state.save_name;
        self.beats_per_minute = track_state.beats_per_minute;

        for track in track_state.track_list.iter().enumerate() {
            let track_details = track.1.clone();
            self.track_list[track.0].set_channel(track_details.0)?;
            self.track_list[track.0].load_saved_state(track_details.1)?;
        }

        Ok(())
    }
}

fn generate_random_project_name() -> Result<String, SystemTimeError> {
    let now = SystemTime::now().duration_since(UNIX_EPOCH)?;
    Ok(format!("Project-{:#?}", now.as_millis()))
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn generate_random_project_name_success() {
        let name = generate_random_project_name().expect("Could not generate project name");
        let name_parts = name.split('-').collect::<Vec<&str>>();
        assert_eq!(name_parts[0], "Project");
        assert_eq!(name_parts[1].len(), 13);
        assert!(name_parts[1].parse::<i64>().is_ok());
    }
}
