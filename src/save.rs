#![allow(dead_code)]

use polodb_core::{bson::doc, Collection, Database, IndexModel, IndexOptions};

use std::error::Error;

use crate::errors::LocalError;
use crate::track::TrackStateToSave;
use crate::ProjectStateToSave;

const DATABASE_FILE_PATH: &str = "db/daseq.db";
const PROJECT_COLLECTION_NAME: &str = "projects";
const PROJECT_COLLECTION_INDEX: &str = "save_name";
const TRACK_COLLECTION_NAME: &str = "tracks";
const TRACK_COLLECTION_INDEX: &str = "save_name";

pub struct Save {
    save_database: Database,
    project_collection: Collection<ProjectStateToSave>,
    track_collection: Collection<TrackStateToSave>,
}

impl Save {
    pub fn new() -> Result<Self, Box<dyn Error>> {
        let save_database = Database::open_file(DATABASE_FILE_PATH)?;

        let project_collection: Collection<ProjectStateToSave> =
            save_database.collection(PROJECT_COLLECTION_NAME);

        project_collection.create_index(IndexModel {
            keys: doc! {
            PROJECT_COLLECTION_INDEX: 1,
            },
            options: Some(IndexOptions {
                unique: Some(true),
                ..Default::default()
            }),
        })?;

        let track_collection: Collection<TrackStateToSave> =
            save_database.collection(TRACK_COLLECTION_NAME);

        track_collection.create_index(IndexModel {
            keys: doc! {
            TRACK_COLLECTION_INDEX: 1,
            },
            options: Some(IndexOptions {
                unique: Some(true),
                ..Default::default()
            }),
        })?;

        Ok(Self {
            save_database,
            project_collection,
            track_collection,
        })
    }

    pub fn save_project(&self, project_to_save: ProjectStateToSave) -> Result<(), Box<dyn Error>> {
        match self
            .project_collection
            .find_one(doc! {"save_name": project_to_save.save_name.clone()})
        {
            Ok(None) => {}
            Ok(Some(_)) => {
                return Err(Box::new(LocalError::SaveProjectAlreadyExists(
                    project_to_save.save_name.clone(),
                )))
            }
            Err(e) => return Err(Box::new(e)),
        };

        let _ = self.project_collection.insert_one(project_to_save)?;

        Ok(())
    }

    pub fn list_saved_projects(&self) -> Result<Vec<String>, Box<dyn Error>> {
        let mut project_list: Vec<String> = vec![];

        let saved_projects = self.project_collection.find(None)?;

        for project in saved_projects {
            match project {
                Ok(saved_state) => {
                    project_list.push(saved_state.save_name);
                }
                Err(e) => return Err(Box::new(e)),
            };
        }

        Ok(project_list)
    }

    pub fn retrieve_project(
        &self,
        save_name: String,
    ) -> Result<ProjectStateToSave, Box<dyn Error>> {
        match self
            .project_collection
            .find_one(doc! {"save_name": save_name.clone()})
        {
            Ok(None) => Err(Box::new(LocalError::SavedProjectNameDoesNotExist(
                save_name,
            ))),
            Ok(Some(project)) => Ok(project),
            Err(e) => Err(Box::new(e)),
        }
    }

    pub fn save_track(&self, track_to_save: TrackStateToSave) -> Result<(), Box<dyn Error>> {
        match self
            .track_collection
            .find_one(doc! {"save_name": track_to_save.save_name.clone()})
        {
            Ok(None) => {}
            Ok(Some(_)) => {
                return Err(Box::new(LocalError::SaveTrackAlreadyExists(
                    track_to_save.save_name.clone(),
                )))
            }
            Err(e) => return Err(Box::new(e)),
        };

        let _ = self.track_collection.insert_one(track_to_save)?;

        Ok(())
    }

    pub fn list_saved_tracks(&self) -> Result<Vec<String>, Box<dyn Error>> {
        let mut track_list: Vec<String> = vec![];

        let saved_tracks = self.track_collection.find(None)?;

        for track in saved_tracks {
            match track {
                Ok(saved_state) => {
                    track_list.push(saved_state.save_name);
                }
                Err(e) => return Err(Box::new(e)),
            };
        }

        Ok(track_list)
    }

    pub fn retrieve_track(&self, save_name: String) -> Result<TrackStateToSave, Box<dyn Error>> {
        match self
            .track_collection
            .find_one(doc! {"save_name": save_name.clone()})
        {
            Ok(None) => Err(Box::new(LocalError::SavedTrackNameDoesNotExist(save_name))),
            Ok(Some(track)) => Ok(track),
            Err(e) => Err(Box::new(e)),
        }
    }
}
