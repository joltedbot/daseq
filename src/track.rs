#![allow(dead_code)]
pub mod step;

use crossbeam_channel::{bounded, Receiver, Sender, TryRecvError};
use serde::{Deserialize, Serialize};
use std::error::Error;
use std::sync::{Arc, Mutex};
use std::thread::{self, JoinHandle};
use std::time::Instant;

use crate::errors::LocalError;
use crate::midi::note::Kind;
use crate::midi::{validate_midi_channel_is_in_range, Midi};
use crate::track::step::*;

const MIDI_CONNECTION_NAME_PREFIX: &str = "track_";
const ERROR_MIDI_MESSAGE_SEND: &str = "Can not send midi message";
const ERROR_NO_CHANNEL_RECIEVER: &str = "The track channel receiver disconnected";
const ERROR_CLOCK_PULSE_NOT_RECEIVED: &str = "The clock pulse channel receiver disconnected";
const ERROR_NO_STOP_TRACK_JOIN_HANDLE: &str =
    "Could not find last thread handle to wait for it to finish";
const TRACK_CHANNEL_STOP_VALUE: u64 = 0;
const MIN_MIDI_CHANNEL: u8 = 1;
const MAX_MIDI_CHANNEL: u8 = 16;
const DEFAULT_NOTE_DUTY_CYCLE: f64 = 0.98;
const CLOCK_PULSES_BETWEEN_STEP_SENDS: u8 = 5; // 24 PPQN is 6 pulses per 16th note step but we send notes on the 6th so we have 5 between notes
const MIN_NUMBER_OF_STEPS_PER_TRACK: u8 = 1;
const MAX_NUMBER_OF_STEPS_PER_TRACK: u8 = 128;

#[derive(Clone, Debug)]
pub struct Track {
    pub name: String,
    pub number_of_steps: u8,
    pub step_list: Vec<Step>,
    pub state: State,
    pub midi_output_port_number: usize,
    pub midi_channel: u8,
    track_thread_sender: Option<Sender<u64>>,
}

#[derive(Clone, Default, Debug, PartialEq)]
pub enum State {
    Running,
    #[default]
    Stopped,
}

#[derive(Clone, Debug, Serialize, Deserialize)]
pub struct TrackStateToSave {
    pub save_name: String,
    pub step_list: Vec<Step>,
}

impl Track {
    pub fn new(
        name: String,
        midi_output_port_number: usize,
        number_of_steps: u8,
        midi_channel: u8,
    ) -> Result<Self, Box<dyn Error>> {
        validate_midi_channel_is_in_range(midi_channel)?;
        Ok(Self {
            name,
            midi_output_port_number,
            midi_channel,
            number_of_steps,
            state: State::Stopped,
            track_thread_sender: None,
            step_list: vec![Default::default(); number_of_steps as usize],
        })
    }

    pub fn set_number_of_steps(&mut self, number_of_steps: u8) -> Result<(), LocalError> {
        if !(MIN_NUMBER_OF_STEPS_PER_TRACK..=MAX_NUMBER_OF_STEPS_PER_TRACK)
            .contains(&number_of_steps)
        {
            return Err(LocalError::TrackStepsOutOfRange {
                min: MIN_NUMBER_OF_STEPS_PER_TRACK,
                max: MAX_NUMBER_OF_STEPS_PER_TRACK,
            });
        }

        self.step_list
            .resize(number_of_steps as usize, Default::default());

        self.number_of_steps = number_of_steps;

        Ok(())
    }

    pub fn set_step(&mut self, number: u8, new_step: Step) -> Result<(), LocalError> {
        if number as usize > self.step_list.len() {
            return Err(LocalError::StepDoesNotExist(number));
        }

        let _ = std::mem::replace(&mut self.step_list[number as usize], new_step);

        Ok(())
    }

    pub fn set_all_steps(&mut self, step_list: Vec<Step>) -> Result<(), LocalError> {
        if !((MIN_NUMBER_OF_STEPS_PER_TRACK as usize)..=(MAX_NUMBER_OF_STEPS_PER_TRACK as usize))
            .contains(&step_list.len())
        {
            return Err(LocalError::TrackStepsOutOfRange {
                min: MIN_NUMBER_OF_STEPS_PER_TRACK,
                max: MAX_NUMBER_OF_STEPS_PER_TRACK,
            });
        }

        self.number_of_steps = step_list.len() as u8;
        self.step_list = step_list;

        Ok(())
    }

    pub fn clear_step(&mut self, number: u8) -> Result<(), LocalError> {
        if number as usize > self.step_list.len() {
            return Err(LocalError::StepDoesNotExist(number));
        }

        let _ = std::mem::take(&mut self.step_list[(number) as usize]);

        Ok(())
    }

    pub fn start(
        &mut self,
        clock_pulse_receiver: Receiver<bool>,
    ) -> Result<JoinHandle<()>, Box<dyn Error>> {
        if self.state == State::Running {
            return Err(Box::new(LocalError::TrackAlreadyRuning));
        }

        let track_connection_name = format!("{}{}", MIDI_CONNECTION_NAME_PREFIX, self.name);

        let midi_output = Arc::new(Mutex::new(Midi::new(
            track_connection_name.as_str(),
            self.midi_output_port_number,
        )?));

        let (track_thread_sender, track_thread_reciever) = bounded(2);
        self.track_thread_sender = Some(track_thread_sender);

        // Create owned values of Track attributes that can be safely passed into the thread in the track_runner
        let track_list = self.step_list.clone();
        let track_channel = self.midi_channel;

        self.state = State::Running;

        Ok(self.track_runner(
            track_list,
            track_thread_reciever,
            clock_pulse_receiver,
            midi_output,
            track_channel,
        ))
    }

    pub fn stop(&mut self) -> Result<(), Box<dyn Error>> {
        if self.state == State::Stopped {
            return Ok(());
        }

        let sender = match self.track_thread_sender.clone() {
            Some(sender) => sender,
            None => return Err(LocalError::TrackNoChannelSenderFound.into()),
        };

        match sender.send(TRACK_CHANNEL_STOP_VALUE) {
            Ok(()) => {
                self.state = State::Stopped;
                Ok(())
            }
            Err(e) => Err(Box::new(e)),
        }
    }

    pub fn set_channel(&mut self, channel: u8) -> Result<(), LocalError> {
        validate_midi_channel_is_in_range(channel)?;

        self.midi_channel = channel;
        Ok(())
    }

    pub fn set_name(&mut self, name: String) {
        self.name = name;
    }

    pub fn get_state_to_save(&self, save_name: String) -> TrackStateToSave {
        TrackStateToSave {
            save_name,
            step_list: self.step_list.clone(),
        }
    }

    pub fn load_saved_state(&mut self, track_state: TrackStateToSave) -> Result<(), LocalError> {
        self.set_all_steps(track_state.step_list)
    }

    fn track_runner(
        &self,
        track_list: Vec<Step>,
        track_thread_reciever: Receiver<u64>,
        clock_pulse_receiver: Receiver<bool>,
        midi_output: Arc<Mutex<Midi>>,
        track_channel: u8,
    ) -> JoinHandle<()> {
        thread::spawn(move || {
            let mut stop_the_track = false;

            loop {
                for step in track_list.clone() {
                    match track_thread_reciever.try_recv() {
                        Err(TryRecvError::Empty) => {}
                        Ok(0) => stop_the_track = true, // Stop the track
                        Ok(_) => {}
                        Err(_) => panic!("{}", ERROR_NO_CHANNEL_RECIEVER),
                    }

                    let note_duration =
                        note_duration_from_clock_pulses(clock_pulse_receiver.clone());

                    let current_thread_handle =
                        send_step_midi(midi_output.clone(), step, track_channel, note_duration);

                    if stop_the_track {
                        current_thread_handle
                            .join()
                            .expect(ERROR_NO_STOP_TRACK_JOIN_HANDLE);

                        return;
                    }
                }
            }
        })
    }
}

fn note_duration_from_clock_pulses(clock_pulse_receiver: Receiver<bool>) -> u64 {
    let mut pulse_counter = 0;
    let clock_pulse_per_step_timer = Instant::now();

    loop {
        clock_pulse_receiver
            .recv()
            .expect(ERROR_CLOCK_PULSE_NOT_RECEIVED);

        if pulse_counter == CLOCK_PULSES_BETWEEN_STEP_SENDS {
            break;
        }

        pulse_counter += 1;
    }

    f64::floor(
        (clock_pulse_per_step_timer.elapsed().as_secs_f64() * 1000000.0) * DEFAULT_NOTE_DUTY_CYCLE,
    ) as u64
}

fn send_step_midi(
    midi_output: Arc<Mutex<Midi>>,
    step: Step,
    track_channel: u8,
    note_duration: u64,
) -> JoinHandle<()> {
    let step_note = step.note.clone();
    let step_control_change = step.control_change.clone();
    let local_midi = midi_output.clone();

    thread::spawn(move || {
        if let Ok(mut unlocked_midi) = local_midi.lock() {
            if step_note.kind != Kind::Rest {
                unlocked_midi
                    .send_note(track_channel, step_note, note_duration)
                    .expect(ERROR_MIDI_MESSAGE_SEND);
            }

            if let Some(control_change) = step_control_change {
                unlocked_midi
                    .send_cc(track_channel, control_change)
                    .expect(ERROR_MIDI_MESSAGE_SEND);
            }
        }
    })
}

#[cfg(test)]
mod tests {
    use super::*;
    use crate::clock::Clock;
    use crate::midi::cc::ControlChange;
    use crate::midi::note::Note;
    use midir::{os::unix::VirtualOutput, MidiOutput};

    // A mock midi object that allows a virutual MidiOutputConnection passed into methods without needing to have a real midi port
    fn midi_test_object() -> Midi {
        let output = MidiOutput::new("test_output").ok().unwrap();
        let output_connection = output.create_virtual("test_port").ok().unwrap();

        Midi { output_connection }
    }

    #[test]
    fn set_step_success() {
        let test_name = "new_test".to_string();
        let test_steps = 2;
        let test_bpm = 120;
        let test_step_note = Note::new(Kind::TieEnd, vec![(64, 54)]).unwrap();
        let test_step_cc = Some(ControlChange::new(10, 10).unwrap());
        let test_clock = Clock::new("Test Clock".to_string(), test_bpm).unwrap();

        let mut test_track = Track::new(test_name.clone(), 0, test_steps, 1).unwrap();

        test_track
            .set_step(0, Step::new(test_step_note.clone(), test_step_cc))
            .expect("Oops!");

        assert_eq!(test_track.name, test_name);
        assert_eq!(test_track.step_list[0].note, test_step_note);
    }

    #[test]
    fn set_step_fail_step_does_not_exist_high() {
        let test_clock = Clock::new("Test Clock".to_string(), 120).unwrap();

        let mut test_track = Track::new("set_step_fail".to_string(), 0, 2, 1).unwrap();

        let test_step = 3;

        match test_track.set_step(test_step, Default::default()) {
            Ok(()) => panic!(
                "Should have failed on test step {} is greater than the size of the step list {}",
                test_step,
                test_track.step_list.len()
            ),
            Err(e) => assert!(e == LocalError::StepDoesNotExist(3)),
        }
    }

    #[test]
    fn clear_step_success() {
        let test_clock = Clock::new("Test Clock".to_string(), 120).unwrap();

        let test_unset_step_note = Note::new(Kind::Rest, vec![]).unwrap();
        let mut test_track = Track::new("set_step_fail".to_string(), 0, 2, 1).unwrap();

        let test_step_note = Note::new(Kind::TieEnd, vec![(64, 54)]).unwrap();
        let test_step_cc = Some(ControlChange::new(10, 10).unwrap());

        test_track
            .set_step(1, Step::new(test_step_note, test_step_cc))
            .expect("Could Note Set Step;");

        assert!(test_track.clear_step(1).is_ok());
        assert_eq!(test_track.step_list[0].note, test_unset_step_note);
    }

    #[test]
    fn clear_step_fail_step_does_not_exist() {
        let test_clock = Clock::new("Test Clock".to_string(), 120).unwrap();

        let mut test_track = Track::new("set_step_fail".to_string(), 0, 2, 1).unwrap();
        assert!(test_track.clear_step(3).is_err());
    }

    #[test]
    fn stop_success() {
        let test_clock = Clock::new("Test Clock".to_string(), 120).unwrap();
        let mut test_track = Track::new("stop_sucess".to_string(), 0, 2, 1).unwrap();

        test_track.state = State::Running;
        let (track_thread_sender, _track_thread_receiver) = bounded(2);
        test_track.track_thread_sender = Some(track_thread_sender);

        assert!(test_track.stop().is_ok());
    }

    #[test]
    fn stop_success_already_running() {
        let test_clock = Clock::new("Test Clock".to_string(), 120).unwrap();
        let mut test_track =
            Track::new("stop_sucess_already_running".to_string(), 0, 2, 1).unwrap();
        test_track.state = State::Stopped;

        assert!(test_track.stop().is_ok());
    }

    #[test]
    fn stop_fail_no_sender() {
        let test_clock = Clock::new("Test Clock".to_string(), 120).unwrap();

        let mut test_track =
            Track::new("stop_sucess_already_running".to_string(), 0, 2, 1).unwrap();
        test_track.state = State::Running;

        assert!(test_track.stop().is_err());
    }
}
